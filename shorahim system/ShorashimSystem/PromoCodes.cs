﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShorashimSystem
{
    class PromoCodes
    {
        private string promoName;
        private string promoDescription;
        private DateTime promoStartDate;
        private double promoDiscountPercent;
        private DateTime promoEndDate;
        public System.Collections.Generic.List<Purchase> promoCodeInPurchase; //הכלה

        public PromoCodes(string promoName, string promoDescription, DateTime promoStartDate, double promoDiscountPercent, DateTime promoEndDate, bool is_new)
        {
            this.promoName = promoName;
            this.promoDescription = promoDescription;
            this.promoStartDate = promoStartDate;
            this.promoDiscountPercent = promoDiscountPercent;
            this.promoEndDate = promoEndDate;
            promoCodeInPurchase = new List<Purchase>();
        }

        public string getPromoName()
        {
            return this.promoName;
        }

        public string getPromoDescription()
        {
            return this.promoDescription;
        }
        public DateTime getPromoStartDate()
        {
            return this.promoStartDate;
        }

        public double getPromoDiscount()
        {
            return this.promoDiscountPercent;
        }

        public DateTime getPromoEndDate()
        {
            return this.promoEndDate;
        }


    }
}
