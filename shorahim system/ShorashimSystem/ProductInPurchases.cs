﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ShorashimSystem
{
    class ProductInPurchases
    {

        private Purchase purchase; //הכלה
        private Product product; //הכלה
        private int quantity;


        public ProductInPurchases(Purchase purchaseToCustomer, Product product, int quantity)
        {
            this.quantity = quantity;
            this.purchase = purchaseToCustomer; //הכלה
            this.product = product; //הכלה
            this.purchase.Products_In_Purchase.Add(this); // הכלה
        }

        public string get_ProductID()
        {
            return this.product.get_productID();
        }
        public void set_Product_In_Purchaes(Product Value)
        {
            this.product = Value;

        }
        public int get_CustomerID()
        {
            return this.purchase.get_purchcaseCustomerID();
        }
        public int get_EmployeeID()
        {
            return this.purchase.get_purchcaseEmployeeID();
        }

        public int get_PurchaseID()
        {
            return this.purchase.getID();
        }

        public int get_Quantity()
        {
            return this.quantity;
        }

        public Product get_Product()
        {
            return this.product;
        }


        public void createProductInPurchase()
        {
            SqlCommand c = new SqlCommand();
            c.CommandText = "EXECUTE dbo.SP_Add_Products_In_Course @Product_ID, @Purchase_ID, @Quantity";
            c.Parameters.AddWithValue("@Product_ID", this.product.get_productID());
            c.Parameters.AddWithValue("@Purchase_ID", this.purchase.getID());
            c.Parameters.AddWithValue("@Quantity", this.quantity);
            SQL_CON SC = new SQL_CON();
            SC.execute_non_query(c);
        }



    }
}
