﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShorashimSystem
{
    public partial class Main_Menu : Form
    {
        int EMP_ID;
        public Main_Menu(int EMP_ID)
        {
            this.EMP_ID = EMP_ID;
            InitializeComponent();
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {
            timer1.Start();

        }

        private void OrderFromSupplierButton_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ClockLabel.Text = DateTime.Now.ToString("HH:mm:ss");
            DayLabel.Text = DateTime.Now.ToString("dddd" + "  dd MMMM yyyy");
        }

        private void PromotionByMailButton_Click(object sender, EventArgs e)
        {
            PromotionByMailing New_PBM = new PromotionByMailing(EMP_ID);
            New_PBM.Enabled = true;//maybe not needed
            New_PBM.Show();
            this.Hide();
        }

        private void ExitButton1_Click(object sender, EventArgs e)
        {

            Form Map = new Map();
            Map.Enabled = true;//maybe not needed
            Map.Show();
            this.Hide();
        }
    }
}



             