﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace ShorashimSystem
{
  
        class ShorashimCourse
        {

            private string courseNumber;
            private string courseName;
            private DateTime courseDateTime;
            private string courseLecturerName;
            private string LecturerPhone;
            private int participantPrice;
            private int costToCompany;
            private string courseLocation;
            private string courseDescription;


            public System.Collections.Generic.List<CustomerInCourse> customersInCourse;
            public System.Collections.Generic.List<ProductInCourse> productsNeeded;

            public ShorashimCourse(string courseNumber, string courseName, DateTime courseDateTime, string courseLecturerName, string LecturerPhone, int participantPrice, int costToCompany, string location, string Description, bool is_new)
            {
                this.courseNumber = courseNumber;
                this.courseName = courseName;
                this.courseDateTime = courseDateTime;
                this.courseLecturerName = courseLecturerName;
                this.LecturerPhone = LecturerPhone;
                this.participantPrice = participantPrice;
                this.costToCompany = costToCompany;
                this.courseLocation = location;
                this.courseDescription = Description;

                productsNeeded = new List<ProductInCourse>();///

                if (is_new)
                {
                    this.create_Course(); // new data to sql
                    Program.Shorashim_Courses.Add(this); // add to courses list in program
                }

            }

            public string get_CourseName()
            {
                return this.courseName;
            }
            public string get_Coursenumber()
            {
                return this.courseNumber;
            }

            public DateTime get_CourseDateTime()
            {
                return this.courseDateTime;
            }

            public string get_courseLecturerName()
            {
                return this.courseLecturerName;
            }

            public string get_LecturerPhone()
            {
                return this.LecturerPhone;
            }



        


            //CRU FUNCTIONS:

            public void create_Course()
            {
                SqlCommand c = new SqlCommand();
                c.CommandText = "EXECUTE dbo.SP_Add_Courses  @CourseNumber, @Course_Name, @Course_DateTime, @Course_Lecturer_Name, @Lecturer_Phone, @Participant_Price, @Cost_To_Copany, @Curse_Location, @Curse_Description ";
                c.Parameters.AddWithValue("@Course_Number", this.courseNumber);
                c.Parameters.AddWithValue("@Course_Name", this.courseName);
                c.Parameters.AddWithValue("@Course_DateTime", this.courseDateTime);
                c.Parameters.AddWithValue("@Course_Lecturer_Name", this.courseLecturerName);
                c.Parameters.AddWithValue("@Lecturer_Phone", this.LecturerPhone);
                c.Parameters.AddWithValue("@Participant_Price", this.participantPrice);
                c.Parameters.AddWithValue("@Cost_To_Copany", this.costToCompany);
                c.Parameters.AddWithValue("@Curse_Location", this.courseLocation);
                c.Parameters.AddWithValue("@Curse_Description", this.courseDescription);
                SQL_CON SC = new SQL_CON();
                SC.execute_non_query(c);
            }

            public void update_Course()
            {
                SqlCommand c = new SqlCommand();
                c.CommandText = "EXECUTE dbo.SP_Update_Courses  @CourseNumber, @Course_Name, @Course_DateTime, @Course_Lecturer_Name, @Lecturer_Phone, @Participant_Price, @Cost_To_Copany, @Curse_Location, @Curse_Description ";
                c.Parameters.AddWithValue("@Course_Number", this.courseNumber);
                c.Parameters.AddWithValue("@Course_Name", this.courseName);
                c.Parameters.AddWithValue("@Course_DateTime", this.courseDateTime);
                c.Parameters.AddWithValue("@Course_Lecturer_Name", this.courseLecturerName);
                c.Parameters.AddWithValue("@Lecturer_Phone", this.LecturerPhone);
                c.Parameters.AddWithValue("@Participant_Price", this.participantPrice);
                c.Parameters.AddWithValue("@Cost_To_Copany", this.costToCompany);
                c.Parameters.AddWithValue("@Curse_Location", this.courseLocation);
                c.Parameters.AddWithValue("@Curse_Description", this.courseDescription);
                SQL_CON SC = new SQL_CON();
                SC.execute_non_query(c);
            }
      
        }
}
