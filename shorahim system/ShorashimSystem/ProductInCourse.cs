﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace ShorashimSystem
{
    class ProductInCourse
    {
        private Product product;
        private ShorashimCourse course;
        private int quantity;

        public ProductInCourse(Product product, ShorashimCourse course, int quantity, bool is_new)
        {
            this.product = product; // הכלה
            this.course = course; //הכלה
            this.quantity = quantity;

            if (is_new)
            {
                this.createProductInCourse();
            }
            this.course.productsNeeded.Add(this);
            this.product.Products_In_Course.Add(this);
        }





        public void createProductInCourse()
        {
            SqlCommand c = new SqlCommand();
            c.CommandText = "EXECUTE dbo.SP_Add_Products_In_Course @Course_Number, @Product_ID, @Quantity";
            c.Parameters.AddWithValue("@Course_Number", this.course.get_Coursenumber());
            c.Parameters.AddWithValue("@Product_ID", this.product.get_productID());
            c.Parameters.AddWithValue("@Quantity", this.quantity);
            SQL_CON SC = new SQL_CON();
            SC.execute_non_query(c);
        }



    }
}
