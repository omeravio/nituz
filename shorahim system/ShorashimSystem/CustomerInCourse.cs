﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ShorashimSystem
{
    class CustomerInCourse
    {
        private Customer customer;
        private ShorashimCourse course;
        private int quantity;

        public CustomerInCourse(Customer customer, ShorashimCourse course, int quantity, bool is_new)
        {
            this.customer = customer; // הכלה
            this.course = course; //הכלה
            this.quantity = quantity;

            if (is_new)
            {
                this.createCustomerInCourse();
            }
            this.course.customersInCourse.Add(this);
            this.customer.customerCourses.Add(this);
        }






        public void createCustomerInCourse()
        {
            SqlCommand c = new SqlCommand();
            c.CommandText = "EXECUTE dbo.SP_Add_Customer_In_Course @Course_Number, @Customer_ID, @Quantity";
            c.Parameters.AddWithValue("@Course_Number", this.course.get_Coursenumber());
            c.Parameters.AddWithValue("@Customer_ID", this.customer.get_customerID());
            c.Parameters.AddWithValue("@Quantity", this.quantity);
            SQL_CON SC = new SQL_CON();
            SC.execute_non_query(c);
        }


    }
}
